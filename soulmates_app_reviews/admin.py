from django.contrib import admin

from soulmates_app_reviews.models import Review

# Register your models here.

admin.site.register(Review)