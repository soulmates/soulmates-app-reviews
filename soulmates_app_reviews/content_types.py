from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.template.loader import render_to_string
from soulmates_app_reviews.models import Review

        

# @start_from {int} - slice query from
# @end_to {int} - slice query to
class RecentReviewsContentType(models.Model):
    model = Review
    start_from = 0
    end_to = 3
    def render(self, **kwargs):
        reviews = self.model.objects.filter(published=True)[self.start_from : self.end_to]
        return render_to_string('reviews/recent.html', {
            'reviews': reviews,
            })
            
    class Meta:
        abstract = True
        verbose_name = _('recent reviews block')
        verbose_name_plural = _('recent reviews block')