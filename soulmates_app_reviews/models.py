from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.db import models

# Create your models here.

@python_2_unicode_compatible  # only if you need to support Python 2
class Review(models.Model):
    # fields
    name = models.CharField(max_length=255, verbose_name=_('author'))
    subtitle = models.CharField(max_length=255, 
                                verbose_name=_('subtitle'),
                                null=True,
                                blank=True)
    text = models.TextField(blank=True, verbose_name = _('review text'))
    published = models.BooleanField(default=True, verbose_name=_('published'))
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_('created at'))
    updated_at = models.DateTimeField(auto_now=True, verbose_name=_('Update at'))
        
    # methods
    def __str__(self):
        return self.name
        
    class Meta:
        verbose_name = _('Review')
        verbose_name_plural = _('Reviews')
        ordering = ['-created_at']
        # abstract = True