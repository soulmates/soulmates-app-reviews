from setuptools import setup, find_packages


setup(
    name="soulmates-app-reviews",
    version='0.1',
    description='common utils modules etc',
    include_package_data = True,
    
    # Author details
    author='Eddy Lazar',
    author_email='eddy.lazar@soulmates.pro',
    license='MIT',
    packages=find_packages(),
    
    install_requires=[
        'django >1.9, <2.0',
    ],
    zip_safe=False
)