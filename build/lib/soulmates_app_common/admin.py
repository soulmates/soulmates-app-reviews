from django.contrib import admin
from common.utils import thumb_img_factory, compose
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _


get_admin_thumb = compose(format_html, thumb_img_factory('image', width=150, height=100, crop='fill'))
get_admin_thumb.short_description = _('thumbnail')

SEO_ADMIN_FIELDS = (_('SEO tags'), {
    'classes': ('collapse', ),
    'fields': ('seo_title', 'seo_keywords', 'seo_description'),
})