import operator
import functools
from cloudinary.utils import cloudinary_url 

def compose(*functions):
    return functools.reduce(lambda f, g: lambda x: f(g(x)), functions, lambda x: x)

def cloudinary_img_factory(opts):
    return lambda img: img.image(**opts) if img else ''

def cloudinary_url_factory(opts):
    return lambda img: cloudinary_url(img.public_id, **opts)[0] if img else ''

    
    
# @name {string} - image field name
# @kwargs - image manipulation options
def thumb_img_factory(name, **kwargs):
    return compose(cloudinary_img_factory(kwargs), operator.attrgetter(name))

# @name {string} - image field name
# @kwargs - image manipulation options
def thumb_url_factory(name, **kwargs):
    return compose(cloudinary_url_factory(kwargs), operator.attrgetter(name))

    